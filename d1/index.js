console.log("Hello B211!");

//Object
/*
	-Object is a data type that is used to represent real world objects
	-It is a collection of related data and/or functionalities
	-Information stored in objects are represented in a "key:value" pair

	key - property of an object
	value - actual data to be stored

	-diferrent data type may be stored in an object's property creating complex data structures
*/
 	
 	//There are 2 ways in creating objects in JS
 		//1. Object Literal Notation 
 				//(let object = {}) **curly braces (object literals)
 		//2. Object Constructor Notation
 				//Object Installation (let object = new Object())


 	//1. Object Literal Notation 
 		//creating objects using initializer or literal notation
 		//camelCase
 		/*
			SyntaX:

				let objectName = {
					keyA : valueA,
					keyB : valueB
				}
 		*/

 		let cellphone = {
 			name: "Nokia 3210",
 			manufactureDate: 1999
 		};

 		//Mini-Activity1
 		console.log("Result from creating objects using literal notation:");
 		console.log(cellphone);

 		let cellphone2 = {
 			name: "Vivo Y69",
 			manufactureDate: 2017
 		};
 		//End of Mini-Activity1

 		console.log("Result from creating objects using literal notation:");
 		console.log(cellphone2);

 		let ninja = {
 			name: "Naruto Uzumaki",
 			village: "Konoha",
 			children: ["Boruto", "Himawari"]
 		};
 		console.log(ninja);


 		//2. Object Constructor Notation
 			//creating objects using a constructor function
 			//creates a reusable "function" to create several objects that have the same data structure

 			/*
				Syntax:

					function objectName(keyA,keyB){
						this.keyA = keyA;
						this.keyB = keyB;
					};
 			*/

 			//"this" keyword refers to the properties within the object
 				//it allows the assignment of new object's properties
 				//by associating them with values received from the constructor function's parameter

 			function Laptop(name, manufactureDate){
 				this.name = name;
 				this.manufactureDate = manufactureDate;
 			}

 			//create an instance object using the Laptop constructor
 			let laptop = new Laptop("Lenovo", 2008);
 			console.log("Result from creating objects using object constructor:");
 			console.log(laptop);

 			//the "new" operator creates an instance of an object (new object)
 			let myLaptop = new Laptop("Macbook Air", 2020);
 			console.log(myLaptop);

 			//Mini-Activity2
 			let myLaptop1 = new Laptop("Dell", 2019);
 			console.log(myLaptop1);

 			let myLaptop2 = new Laptop("Samsung", 2014);
 			console.log(myLaptop2);

 			let myLaptop3 = new Laptop("HP", 2018);
 			console.log(myLaptop3);
 			//End of Mini-Activity2

 			//invoking a laptop function instead of creating new instance (without "new" keyword) will give an undefined because "new" keyword served as return
 			let oldLaptop = Laptop("Portal R2E", 1980);
 			console.log(oldLaptop);

 	//Create empty objects

		let computer = {};
		let myComputer = new Object();

		console.log(computer);
		console.log(myComputer);

 	//How to access object properties

 		//Using dot notation (recommended)
 			console.log("Result from dot notation: " + myLaptop.name);

 		//Using square bracket notation
 			console.log("Result from square bracket notation: " + myLaptop["name"]);

 	//Accessing Array of Objects
 		//accessing object properties using the square bracket notaiona nd array indexes can cause confusion

 		let arrayObject = [laptop, myLaptop];
 		console.log(arrayObject[0]["name"]); //Lenovo

 		console.log(arrayObject[0].name); //Lenovo

 	//Initializing/ Adding/ Deleting/ Reassigning Object Properties

 	//Create an Object using Object Literals
	let car = {};
	console.log("Current value of car object: ");
	console.log(car); //{}

	//Initializing/adding object properties
	car.name = "Honda Civic";
	console.log("Result fromm adding properties using dot notation:");
	console.log(car);

	//Initializing/adding object properties using bracket noatation (NOT RECOMMENDED)
	console.log("Result fromm adding properties using square bracket notation:");
	car["manufacture date"] = 2009;
	console.log(car);

	//Deleting object properties
		delete car["manufacture date"];
		console.log("Result fromm deleteing properties:");
		console.log(car);

	car["manufactureDate"] = 2019;
	console.log(car);

	//Reassigning object property values
	car.name = "Toyota Vios";
	console.log("Result fromm reassigning property values:");
	console.log(car);

	//Object Methods
		//a method is a function whisch is a property of an object

		let person = {
			name: "John",
			talk: function(){
				console.log("Hello my name is " + this.name);
			}
		}
		console.log(person);
		console.log("Result from object methods:");
		person.talk();

		//initialize/add object property
		person.walk = function(steps){
			console.log(this.name + "walked" + steps + "steps forward");
		};
		console.log(person);
		person.walk(50);

		//Methods are useful for creating resusable function that performs tasks related to objects

		let friend = {
			firstName: "Joe",
			lastName: "Smith",
			address: {
				city: "Austin",
				country: "Texas"
			},
			emails: ["joe@mail.com", "joesmith@mail.xyz"],
			introduce: function(){
				console.log("Hello my name is " + this.firstName + " " + this.lastName + " " + "I live in " + this.address.city + "," + this.address.country)
			}
			
		}
		friend.introduce();

		//Real World Application of Objects
		/*
			Scenario:
			1. We would like to create a game that would have several Pokemon interact with each other
			2. Every Pokemon would have the same set of stats, properties and function

			Stats:
			name:
			level:
			health: level*2
			attack: level
		*/

		//create an object cinstructor to lessen the process in creating the pokemon

		function Pokemon(name, level){
			//properties
			this.name = name;
			this.level = level;
			this.health = level*2;
			this.attack = level;
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);

				//Mini-activity2 solution
				// target.health = target.health - this.attack;
				target.health -= this.attack;

				console.log(target.name + "health is now reduced to" + target.health);
				if (target.health <=0){
					target.faint()
					}
				}
				this.faint = function(){
					console.log(this.name + " fainted.")
				}
			}

			let pikachu = new Pokemon ("Pikachu", 88);
			console.log(pikachu);

			let rattata = new Pokemon ("Rattata", 10);
			console.log(rattata);



